﻿using BuildClass;
using System;
using System.IO;
using System.Reflection;

namespace buildclass
{
    class Program
    {
        static void Main(string[] args)
        {

            //Constants for fragment source files
            string _resource_top = "build_class.fragment-top.txt";
            string _resource_middle = "build_class.fragment-middle.txt";
            string _resource_bottom = "build_class.fragment-bottom.txt";
            string _resource_resources = "build_class.fragment-resources.txt";
            string _resource_outputs = "build_class.fragment-outputs.txt";

            const string _template_default = "class.template";

            //string variables for fragment content
            string fragment_top = "";
            string fragment_middle = "";
            string fragment_bottom = "";
            string fragment_resources = "";
            string fragment_outputs = "";
            string fragment_temp = "";

            string resourcename = "";

            string output_filename = _template_default;


            string finalstring = "";

            

            int class_size = 1;



            InputArguments arguments = new InputArguments(args);

           
            if (!arguments.Contains("-t"))
                Console.WriteLine(string.Format("t is not set. Will default to {0}", _template_default));
            else
                output_filename = arguments["-t"];


            if (!arguments.Contains("-s"))
                Console.WriteLine("s is not set. Will default to 1");
            else
                class_size = int.Parse( arguments["-s"]);


            //docker flag, use the alternate fragments
            if (arguments.Contains("-d"))
            {
                _resource_top = "build_class.docker.fragment-top.txt";
                _resource_middle = "build_class.docker.fragment-middle.txt";
                _resource_bottom = "build_class.docker.fragment-bottom.txt";
                _resource_resources = "build_class.docker.fragment-resources.txt";
                _resource_outputs = "build_class.docker.fragment-outputs.txt";
            }

            var assembly = Assembly.GetExecutingAssembly();

            //Read in top fragment
            resourcename = _resource_top;
            using (Stream stream = assembly.GetManifestResourceStream(resourcename))
            using (StreamReader reader = new StreamReader(stream))
            {
                fragment_top = reader.ReadToEnd();
            }
            finalstring = string.Concat(finalstring, fragment_top);

            //Resources fragment(s)
            for (int s = 1; s <= class_size; s++)
            {
                //Read in resources fragment
                resourcename = _resource_resources;
                using (Stream stream = assembly.GetManifestResourceStream(resourcename))
                using (StreamReader reader = new StreamReader(stream))
                {
                    fragment_resources = reader.ReadToEnd();
                }
                               

                //fragment_resources replace xx with student number
                fragment_temp = fragment_resources.Replace("XX", s.ToString());

                finalstring = string.Concat(finalstring, fragment_temp);

                //if not last fragment, add a comma
                if(s<class_size)
                    finalstring = string.Concat(finalstring, ",\n");
            }

            //Read in middle fragment
            resourcename = _resource_middle;
            using (Stream stream = assembly.GetManifestResourceStream(resourcename))
            using (StreamReader reader = new StreamReader(stream))
            {
                fragment_middle = reader.ReadToEnd();
            }
            finalstring = string.Concat(finalstring, fragment_middle);

            //if its not docker do the outputs for the EIP
            if (!arguments.Contains("-d"))
            {
                //Output fragment(s)
                for (int s = 1; s <= class_size; s++)
                {
                    //Read in Outputs fragment
                    resourcename = _resource_outputs;
                    using (Stream stream = assembly.GetManifestResourceStream(resourcename))
                    using (StreamReader reader = new StreamReader(stream))
                    {
                        fragment_outputs = reader.ReadToEnd();
                    }


                    //fragment_resources replace xx with student number
                    fragment_temp = fragment_outputs.Replace("XX", s.ToString());

                    finalstring = string.Concat(finalstring, fragment_temp);

                    //if not last fragment, add a comma
                    if (s < class_size)
                        finalstring = string.Concat(finalstring, ",\n");
                }
            }


            //Read in bottom fragment
            resourcename = _resource_bottom;
            using (Stream stream = assembly.GetManifestResourceStream(resourcename))
            using (StreamReader reader = new StreamReader(stream))
            {
                fragment_bottom = reader.ReadToEnd();
            }
            finalstring = string.Concat(finalstring, fragment_bottom);



            System.IO.File.WriteAllText(output_filename, finalstring);


        }
    }
}
