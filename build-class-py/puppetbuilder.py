﻿from dockerpuppetbase import DockerPuppetBase
from troposphere import Ref, Tags, FindInMap, Base64, Output
from troposphere.ec2 import SecurityGroup, Instance, EIP

class PuppetBuilder(DockerPuppetBase):
    """description of class"""

    def puppet_master_userdata(self):
        return "\n".join([
            "#!/bin/bash -v",
            "#should be installed already, but in case",
            "sudo apt-get install -y curl",
            "sudo apt-get install -y openssh-server",
            "sudo apt-get install -y ca-certificates",
            "#setup the default answers, change the name to whatever yours is",
            "sudo debconf-set-selections <<< \"postfix postfix/mailname string ec2-52-26-201-71.us-west-2.compute.amazonaws.com\"",
            "sudo debconf-set-selections <<< \"postfix postfix/main_mailer_type string 'Internet Site'\"",
            "sudo apt-get install -y postfix",
            "#get gitlab",
            "curl https://packages.gitlab.com/install/repositories/gitlab/gitlab-ce/script.deb.sh | sudo bash",
            "sudo apt-get install -y gitlab-ce",
            "_myip=\"$(curl http://169.254.169.254/latest/meta-data/public-ipv4)\"",
            "sudo sed -i \"/^external_url/s/external_url.*/external_url 'http:\/\/${_myip}'/\" /etc/gitlab/gitlab.rb",
            "sudo gitlab-ctl reconfigure"
            ])

    def puppet_sgs(self):
        return [
            SecurityGroup(
                "PuppetSecurityGroup",
                GroupDescription="Enable all ports relating to Puppet Master, PuppetDB etc",
                SecurityGroupIngress=[
                    self.build_sg_rule(self.TCP, self.HTTP, self.HTTP, self.ALL_IP),
                    self.build_sg_rule(self.TCP, self.HTTPS, self.HTTPS, self.ALL_IP),
                    self.build_sg_rule(self.TCP, self.SSH, self.SSH, self.ALL_IP),
                    self.build_sg_rule(self.TCP, "5432", "5432", self.ALL_IP),
                    self.build_sg_rule(self.TCP, "4433", "4433", self.ALL_IP),
                    self.build_sg_rule(self.TCP, "8081", "8081", self.ALL_IP),
                    self.build_sg_rule(self.TCP, "8140", "8140", self.ALL_IP),
                    self.build_sg_rule(self.TCP, "3000", "3000", self.ALL_IP),
                    self.build_sg_rule(self.TCP, "4567", "4567", self.ALL_IP),
                    self.build_sg_rule(self.TCP, "61613", "61613", self.ALL_IP),
                    self.build_sg_rule(self.TCP, "4435", "4435", self.ALL_IP)
                    ]
                )
            ]

    def puppet_master_instance(self, student):
        instance = Instance(
            "PuppetMaster{}".format(student),
            InstanceType=Ref(self.instance_type),
            SecurityGroups=[],
            Tags=Tags(
                Name="Puppet Master",
                Student="Student{}".format(student)),
            KeyName=Ref(self.keyname),
            ImageId=FindInMap("RegionMap", Ref("AWS::Region"), "AMI"),
            UserData=Base64(self.puppet_master_userdata()))
        self.append_sgs_to_instance(instance, self.sec_groups)
        return instance

    def puppet_agent_instance(self, student):
        instance = Instance(
            "PuppetAgent{}".format(student),
            InstanceType=Ref(self.instance_type),
            SecurityGroups=[],
            Tags=Tags(
                Name="Puppet Agent",
                Student="Student{}".format(student)),
            KeyName=Ref(self.keyname),
            ImageId=FindInMap("RegionMap", Ref("AWS::Region"), "AMI"))
        self.append_sgs_to_instance(instance, self.sec_groups)
        return instance

    def initialise_template(self):
        super().initialise_template()
        self.sec_groups = self.puppet_sgs()
        for sg in self.sec_groups:
            self.template.add_resource(sg)

    def build_student_resources(self, student):
        puppet_master = self.template.add_resource(self.puppet_master_instance(student))
        master_eip = self.template.add_resource(EIP(
            "MasterEIP{}".format(student),
            InstanceId=Ref(puppet_master)))

        puppet_agent = self.template.add_resource(self.puppet_agent_instance(student))
        agent_eip = self.template.add_resource(EIP(
            "AgentEIP{}".format(student),
            InstanceId=Ref(puppet_agent)))

        #outputs
        self.template.add_output(Output(
            "PuppetMasterIP{}".format(student),
            Value=Ref(master_eip),
            Description="Public IP of Puppet master {}".format(student)))
        self.template.add_output(Output(
            "PuppetAgentIP{}".format(student),
            Value=Ref(agent_eip),
            Description="Public IP of Puppet agent {}".format(student)))
