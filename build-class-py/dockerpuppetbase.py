﻿from templatebuilderbase import TemplateBuilderBase
from abc import ABCMeta, abstractmethod
from troposphere import Parameter

class DockerPuppetBase(TemplateBuilderBase):
    """description of class"""

    def get_region_map(self):
        return {
            'eu-west-1': {'AMI': 'ami-47a23a30'},
            'us-east-1': {'AMI': 'ami-d05e75b8'},
            'us-west-2': {'AMI': 'ami-5189a661'},
            'us-west-1': {'AMI': 'ami-df6a8b9b'},
            'eu-central-1': {'AMI': 'ami-accff2b1'},
            'ap-southeast-1': {'AMI': 'ami-96f1c1c4'},
            'ap-northeast-1': {'AMI': 'ami-936d9d93'},
            'ap-southeast-2': {'AMI': 'ami-69631053'},
            'sa-east-1': {'AMI': 'ami-4d883350'}
            }

    def initialise_template(self):

        self.keyname = Parameter(
            "KeyName",
            Description="Name of an existing EC2 KeyPair to enable "
                          "SSH access to the instance",
            Type="String",
            Default="classkey")

        self.template.add_parameter(self.keyname)
        self.instance_type = self.build_instancetype_param("InstanceType", ["m3.medium", "m3.large"], 1)
        self.template.add_parameter(self.instance_type)

        map = self.get_region_map()
        self.template.add_mapping("RegionMap", map)
