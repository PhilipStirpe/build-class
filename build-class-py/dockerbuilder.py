﻿from dockerpuppetbase import DockerPuppetBase
from troposphere import Ref, Tags, FindInMap, Base64
from troposphere.ec2 import SecurityGroup, Instance

class DockerBuilder(DockerPuppetBase):
    """description of class"""

    def docker_sgs(self):
        return [
            SecurityGroup(
                "DockerSecurityGroup",
                GroupDescription="Enable all TCP ports",
                SecurityGroupIngress=[
                    self.build_sg_rule(self.TCP, "0", "65535", self.ALL_IP)
                    ])
            ]

    def get_userdata(self):
        #and if we include boto, this could be grabbed from s3
        return "\n".join([
            "#!/bin/bash",
            "#update all",
            "yum update -y",
            "yum clean all",
            "#install maven",
            "wget http://apache.mirrors.ionfish.org/maven/maven-3/3.3.3/binaries/apache-maven-3.3.3-bin.tar.gz",
            "tar xzf apache-maven-3.3.3-bin.tar.gz -C /usr/local",
            "ln -s /usr/local/apache-maven-3.3.3 /usr/local/maven",
            "ln -s /usr/local/maven/bin/mvn /bin/mvn",
            "#install git",
            "yum install -y git",
            "#clone the repository",
            "cd /home/ec2-user/",
            "git clone https://bitbucket.org/kizzie/qadockerint-exercises.git",
            "git clone https://bitbucket.org/kizzie/hello-scalatra.git",
            "git clone https://bitbucket.org/kizzie/qadockerint-eventsproject.git",
            "chown -R ec2-user:ec2-user qadockerint-exercises",
            "chown -R ec2-user:ec2-user hello-scalatra",
            "chown -R ec2-user:ec2-user qadockerint-eventsproject"
            ])

    def initialise_template(self):
        super().initialise_template()
        region = self.build_region_param(["eu-west-1"], 0)
        self.template.add_parameter(region)
        self.sec_groups = self.docker_sgs()
        for sec_group in self.sec_groups:
            self.template.add_resource(sec_group)

    def build_student_resources(self, student):
        instance = Instance(
            "Student{}".format(student),
            InstanceType=Ref(self.instance_type),
            SecurityGroups=[],
            Tags=Tags(Name="Student{}".format(student)),
            KeyName=Ref(self.keyname),
            ImageId=FindInMap("RegionMap", Ref("AWS::Region"), "AMI"),
            UserData=Base64(self.get_userdata()))
        self.append_sgs_to_instance(instance, self.sec_groups)
        self.template.add_resource(instance)
