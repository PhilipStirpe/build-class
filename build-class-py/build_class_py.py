﻿import argparse
from dockerbuilder import DockerBuilder
from puppetbuilder import PuppetBuilder
from webserverdemobuilder import WebServerDemoBuilder

def build_template(filename, size, display, docker, web):
    """Build a Puppet, Docker or WebServer Cloudformation template based on
    the arguments passed in
    """

    if docker:
        builder = DockerBuilder(size)
    elif web:
        builder = WebServerDemoBuilder(size)
    else:
        builder = PuppetBuilder(size)

    builder.build_template()
    template = builder.the_template

    if display:
        print(template.to_json())

    template_file = open(filename, "w")
    template_file.write(template.to_json())

def main():
    """Parse command line args then invoke build_template
    """

    parser = argparse.ArgumentParser()
    parser.add_argument("-t", "--template",
                        help="name for the generated Cloudformation template",
                        default="class.template")
    parser.add_argument("-s", "--size",
                        help="class size",
                        type=int,
                        default=1)
    parser.add_argument("-p", "--print",
                        help="display the template",
                        action="store_true")
    group = parser.add_mutually_exclusive_group()
    group.add_argument("-d", "--docker",
                       help="build docker template",
                       action="store_true")
    group.add_argument("-w", "--webserver",
                       help="Build out a simple webserver template",
                       action="store_true")
    args = parser.parse_args()

    build_template(
        args.template, args.size, args.print, args.docker, args.webserver)



if __name__ == "__main__":
    main()
