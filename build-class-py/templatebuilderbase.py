﻿from abc import ABCMeta, abstractmethod
from troposphere import Template, Parameter, Ref
from troposphere.ec2 import SecurityGroupRule

class TemplateBuilderBase(metaclass=ABCMeta):
    """description of class"""


    def build_sg_rule(self, protocol, fromPort, toPort, cidr):
        return SecurityGroupRule(
            IpProtocol = protocol,
            FromPort = fromPort,
            ToPort = toPort,
            CidrIp = cidr)

    def build_region_param(self, allowed_regions, index_of_default):
        descriptions = []
        for i in range(0, len(allowed_regions)):
            description = "{}"
            if i == index_of_default:
                description += " (Default)"
            descriptions.append(description.format(allowed_regions[i]))

        return Parameter(
            "Region",
            Type="String",
            Default=allowed_regions[index_of_default],
            Description=" | ".join(descriptions),
            AllowedValues=allowed_regions)

    def build_instancetype_param(self, name, allowed_types, index_of_default):
        return Parameter(
            name,
            Type="String",
            Default = allowed_types[index_of_default],
            AllowedValues = allowed_types)

    def append_sgs_to_instance(self, instance, sec_groups):
        for sec_group in sec_groups:
            instance.SecurityGroups.append(Ref(sec_group))

    def __init__(self, size):
        self.size = size
        self.__dict__["TCP"] = "tcp"
        self.__dict__["HTTP"] = "80"
        self.__dict__["HTTPS"] = "443"
        self.__dict__["SSH"] = "22"
        self.__dict__["ALL_IP"] = "0.0.0.0/0"

    @abstractmethod
    def build_student_resources(self, student):
        pass

    @abstractmethod
    def initialise_template(self):
        pass

    def build_template(self):
        self.template = Template()
        self.template.add_version("2010-09-09")
        self.template.add_description("Builds classroom users for cloud classrooms")

        self.initialise_template()
        for student in range(1, self.size + 1):
            self.build_student_resources(student)

        return self.template

    def get_template(self):
        return self.template

    the_template = property(get_template)