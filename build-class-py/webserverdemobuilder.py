﻿from abc import ABCMeta
from templatebuilderbase import TemplateBuilderBase
from troposphere import (Parameter, Ref, FindInMap, Tags,
                        Base64, Join, Output, GetAtt)
from troposphere.ec2 import SecurityGroup, Instance
from troposphere.route53 import RecordSetType

class WebServerDemoBuilder(TemplateBuilderBase):
    """description of class"""

    def web_sgs(self):
        return [
            SecurityGroup(
                "WebServerSecurityGroup",
                GroupDescription="Enable HTTP(S) and SSH",
                SecurityGroupIngress=[
                    self.build_sg_rule(self.TCP, self.HTTP, self.HTTP, self.ALL_IP),
                    self.build_sg_rule(self.TCP, self.HTTPS, self.HTTPS, self.ALL_IP),
                    self.build_sg_rule(self.TCP, self.SSH, self.SSH, self.ALL_IP)
                    ])
            ]

    def web_region_map(self):
        return {
            'eu-west-1': {'AMI': 'ami-bff32ccc'}
            }

    def webserver_userdata(self):
        return "\n".join([
            "#!/bin/bash",
            "wget -q --tries=10 --timeout=20 http://google.com",
            "yum update -y",
            "yum -y install httpd php",
            "chkconfig httpd on",
            "/etc/init.d/httpd start",
            "cd /var/www/html",
            "wget https://d2lrzjb0vjvpn5.cloudfront.net/sys-ops/v2.3/lab-1-compute-linux/static/ec2-info.zip",
            "unzip ec2-info.zip",
            "echo 'UserData has been successfully executed.' >> /home/ec2-user/result",
            #"domain={}.".format(domain),
            #"dns=student{}.{}.$domain".format(student, event),
            #"zoneid=$(aws route53 list-hosted-zones-by-name --query \"HostedZones[?Name == '$domain'].Id\" --output text)",
            #"pubIp=$(curl http://169.254.169.254/latest/meta-data/public-ipv4)",
            #"aws route53 change-resource-record-sets --hosted-zone-id $zoneid --cli-input-json '{ \"ChangeBatch\": { \"Changes\": [ { \"Action\": \"CREATE\", \"ResourceRecordSet\": { \"Name\": \"'$dns'\", \"Type\": \"A\", \"TTL\": 300, \"ResourceRecords\": [ { \"Value\": \"'$pubIp'\" } ] } } ] } }'"
            ])

    def initialise_template(self):
        self.keyname = Parameter(
            "KeyName",
            Description="Name of an existing EC2 KeyPair to enable "
                        "SSH access to the instance",
            Type="String",
            Default="classkey")

        self.instance_type = self.build_instancetype_param("WebServerInstanceType",
            ["t2.micro", "t2.small", "t2.medium"], 0)

        self.region = self.build_region_param(["eu-west-1"], 0)

        self.hosted_zone = Parameter(
            "HostedZone",
            Description="The DNS name of an existing Amazon Route 53 "
                        "hosted zone",
            Type="String")

        self.event_id = Parameter(
            "EventId",
            Description="The DNS-compliant identifier for this event",
            AllowedPattern="[a-z\\d]+",
            ConstraintDescription="Must be all lower case or numeric",
            Type="String")

        self.template.add_parameter(self.keyname)
        self.template.add_parameter(self.instance_type)
        self.template.add_parameter(self.region)
        self.template.add_parameter(self.hosted_zone)
        self.template.add_parameter(self.event_id)
    
        map = self.web_region_map()
        self.template.add_mapping("RegionMap", map)

        for sec_group in self.web_sgs():
            self.template.add_resource(sec_group)

    def build_student_resources(self, student):
        instance = Instance(
            "WebServer{}".format(student),
            InstanceType=Ref(self.instance_type),
            SecurityGroups=[],
            Tags=Tags(
                Name = "Web Server {}".format(student),
                Student = "Student{}".format(student)),
            KeyName=Ref(self.keyname),
            ImageId=FindInMap("RegionMap", Ref("AWS::Region"), "AMI"),
            UserData=Base64(self.webserver_userdata()))
        self.append_sgs_to_instance(instance, self.web_sgs())
        self.template.add_resource(instance)

        recordset = self.template.add_resource(RecordSetType(
            "DnsName{}".format(student),
            HostedZoneName=Join("", [Ref(self.hosted_zone), "."]),
            Comment="DNS Name for student {}".format(student),
            Name=Join("", ["student", student, ".", Ref(self.event_id), ".",
                 Ref(self.hosted_zone), "."]),
            Type="A",
            TTL="300",
            ResourceRecords=[GetAtt(instance.title, "PublicIp")]
            ))

        self.template.add_output(Output(
            "WebServerDNS{}".format(student),
            Value=Ref(recordset),
            Description="DNS Name of Web Server {}".format(student)))
