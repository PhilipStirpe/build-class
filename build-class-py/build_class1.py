﻿import argparse
import troposphere
from troposphere import (Template, Parameter, Ref, Base64, 
                         FindInMap, Output, Tags, GetAtt, Join)
from troposphere.ec2 import SecurityGroup, SecurityGroupRule, Instance, EIP
from troposphere.route53 import RecordSetType

#constants
TCP = "tcp"
HTTP = "80"
HTTPS = "443"
SSH = "22"
ALL_IP = "0.0.0.0/0"

#utilities
def build_sg_rule(protocol, from_port, to_port, cidr):
    return SecurityGroupRule(
        IpProtocol=protocol,
        FromPort=from_port,
        ToPort=to_port,
        CidrIp=cidr)

def build_region_param(allowed_regions, index_of_default):
    descriptions = []
    for i in range(0, len(allowed_regions)):
        description = "{}"
        if i == index_of_default:
            description += " (Default)"
        descriptions.append(description.format(allowed_regions[i]))

    return Parameter(
        "Region",
        Type="String",
        Default=allowed_regions[index_of_default],
        Description=" | ".join(descriptions),
        AllowedValues=allowed_regions)

def build_instancetype_param(allowed_types, index_of_default):
    return Parameter(
        "InstanceTypeParameter",
        Type="String",
        Default=allowed_types[index_of_default],
        AllowedValues=allowed_types)

def append_sgs_to_instance(instance, sec_groups):
    for sec_group in sec_groups:
        instance.SecurityGroups.append(Ref(sec_group))

def default_region_map():
    return {
        'eu-west-1': {'AMI': 'ami-47a23a30'},
        'us-east-1': {'AMI': 'ami-d05e75b8'},
        'us-west-2': {'AMI': 'ami-5189a661'},
        'us-west-1': {'AMI': 'ami-df6a8b9b'},
        'eu-central-1': {'AMI': 'ami-accff2b1'},
        'ap-southeast-1': {'AMI': 'ami-96f1c1c4'},
        'ap-northeast-1': {'AMI': 'ami-936d9d93'},
        'ap-southeast-2': {'AMI': 'ami-69631053'},
        'sa-east-1': {'AMI': 'ami-4d883350'}
        }

#puppet resources
def puppet_region_map():
    return default_region_map()

def puppet_master_userdata():
    return "\n".join([
        "#!/bin/bash -v",
        "#should be installed already, but in case",
        "sudo apt-get install -y curl",
        "sudo apt-get install -y openssh-server",
        "sudo apt-get install -y ca-certificates",
        "#setup the default answers, change the name to whatever yours is",
        "sudo debconf-set-selections <<< \"postfix postfix/mailname string ec2-52-26-201-71.us-west-2.compute.amazonaws.com\"",
        "sudo debconf-set-selections <<< \"postfix postfix/main_mailer_type string 'Internet Site'\"",
        "sudo apt-get install -y postfix",
        "#get gitlab",
        "curl https://packages.gitlab.com/install/repositories/gitlab/gitlab-ce/script.deb.sh | sudo bash",
        "sudo apt-get install -y gitlab-ce",
        "_myip=\"$(curl http://169.254.169.254/latest/meta-data/public-ipv4)\"",
        "sudo sed -i \"/^external_url/s/external_url.*/external_url 'http:\/\/${_myip}'/\" /etc/gitlab/gitlab.rb",
        "sudo gitlab-ctl reconfigure"
        ])

def puppet_sgs():
    return [
        SecurityGroup(
            "PuppetSecurityGroup",
            GroupDescription="Enable all ports relating to Puppet Master, PuppetDB etc",
            SecurityGroupIngress=[
                build_sg_rule(TCP, HTTP, HTTP, ALL_IP),
                build_sg_rule(TCP, HTTPS, HTTPS, ALL_IP),
                build_sg_rule(TCP, SSH, SSH, ALL_IP),
                build_sg_rule(TCP, "5432", "5432", ALL_IP),
                build_sg_rule(TCP, "4433", "4433", ALL_IP),
                build_sg_rule(TCP, "8081", "8081", ALL_IP),
                build_sg_rule(TCP, "8140", "8140", ALL_IP),
                build_sg_rule(TCP, "3000", "3000", ALL_IP),
                build_sg_rule(TCP, "4567", "4567", ALL_IP),
                build_sg_rule(TCP, "61613", "61613", ALL_IP),
                build_sg_rule(TCP, "4435", "4435", ALL_IP)
                ])
        ]

def puppet_master_instance(student, instance_type, sec_groups, keyname):
    instance = Instance(
        "PuppetMaster{}".format(student),
        InstanceType=Ref(instance_type),
        SecurityGroups=[],
        Tags=Tags(
            Name="Puppet Master",
            Student="Student{}".format(student)),
        KeyName=Ref(keyname),
        ImageId=FindInMap("RegionMap", Ref("AWS::Region"), "AMI"),
        UserData=Base64(puppet_master_userdata()))
    append_sgs_to_instance(instance, sec_groups)
    return instance

def puppet_agent_instance(student, instance_type, sec_groups, keyname):
    instance = Instance(
        "PuppetAgent{}".format(student),
        InstanceType=Ref(instance_type),
        SecurityGroups=[],
        Tags=Tags(
            Name="Puppet Agent",
            Student="Student{}".format(student)),
        KeyName=Ref(keyname),
        ImageId=FindInMap("RegionMap", Ref("AWS::Region"), "AMI"))
    append_sgs_to_instance(instance, sec_groups)
    return instance

def build_puppet_resources(template, student, instance_type, sec_groups, keyname):
    puppet_master = template.add_resource(puppet_master_instance(student, instance_type, sec_groups, keyname))
    master_eip = template.add_resource(EIP(
        "MasterEIP{}".format(student),
        InstanceId=Ref(puppet_master)))

    puppet_agent = template.add_resource(puppet_agent_instance(student, instance_type, sec_groups, keyname))
    agent_eip = template.add_resource(EIP(
        "AgentEIP{}".format(student),
        InstanceId=Ref(puppet_agent)))

    #outputs
    template.add_output(Output(
        "PuppetMasterIP{}".format(student),
        Value=Ref(master_eip),
        Description="Public IP of Puppet master {}".format(student)))
    template.add_output(Output(
        "PuppetAgentIP{}".format(student),
        Value=Ref(agent_eip),
        Description="Public IP of Puppet agent {}".format(student)))

#docker resources
def docker_sgs():
    return [
        SecurityGroup(
            "DockerSecurityGroup",
            GroupDescription="Enable all TCP ports",
            SecurityGroupIngress=[
                build_sg_rule(TCP, "0", "65535", ALL_IP)
                ])
        ]

def docker_region_map():
    return default_region_map()

def docker_userdata():
    return "\n".join([
        "#!/bin/bash",
        "#update all",
        "yum update -y",
        "yum clean all",
        "#install maven",
        "wget http://apache.mirrors.ionfish.org/maven/maven-3/3.3.3/binaries/apache-maven-3.3.3-bin.tar.gz",
        "tar xzf apache-maven-3.3.3-bin.tar.gz -C /usr/local",
        "ln -s /usr/local/apache-maven-3.3.3 /usr/local/maven",
        "ln -s /usr/local/maven/bin/mvn /bin/mvn",
        "#install git",
        "yum install -y git",
        "#clone the repository",
        "cd /home/ec2-user/",
        "git clone https://bitbucket.org/kizzie/qadockerint-exercises.git",
        "git clone https://bitbucket.org/kizzie/hello-scalatra.git",
        "git clone https://bitbucket.org/kizzie/qadockerint-eventsproject.git",
        "chown -R ec2-user:ec2-user qadockerint-exercises",
        "chown -R ec2-user:ec2-user hello-scalatra",
        "chown -R ec2-user:ec2-user qadockerint-eventsproject"
        ])

def build_docker_resources(template, student, instance_type, sec_groups, keyname):
    instance = Instance(
        "Student{}".format(student),
        InstanceType=Ref(instance_type),
        SecurityGroups=[],
        Tags=Tags(Name="Student{}".format(student)),
        KeyName=Ref(keyname),
        ImageId=FindInMap("RegionMap", Ref("AWS::Region"), "AMI"),
        UserData=Base64(docker_userdata()))
    append_sgs_to_instance(instance, sec_groups)
    template.add_resource(instance)

#webserver resources
def web_sgs():
    return [
        SecurityGroup(
            "WebServerSecurityGroup",
            GroupDescription="Enable HTTP(S) and SSH",
            SecurityGroupIngress=[
                build_sg_rule(TCP, HTTP, HTTP, ALL_IP),
                build_sg_rule(TCP, HTTPS, HTTPS, ALL_IP),
                build_sg_rule(TCP, SSH, SSH, ALL_IP)
                ])
        ]

def web_region_map():
    return {
        'eu-west-1': {'AMI': 'ami-bff32ccc'}
        }

def webserver_userdata():
    return "\n".join([
        "#!/bin/bash",
        "wget -q --tries=10 --timeout=20 http://google.com",
        "yum update -y",
        "yum -y install httpd php",
        "chkconfig httpd on",
        "/etc/init.d/httpd start",
        "cd /var/www/html",
        "wget https://d2lrzjb0vjvpn5.cloudfront.net/sys-ops/v2.3/lab-1-compute-linux/static/ec2-info.zip",
        "unzip ec2-info.zip",
        "echo 'UserData has been successfully executed.' >> /home/ec2-user/result",
        #"domain={}.".format(domain),
        #"dns=student{}.{}.$domain".format(student, event),
        #"zoneid=$(aws route53 list-hosted-zones-by-name --query \"HostedZones[?Name == '$domain'].Id\" --output text)",
        #"pubIp=$(curl http://169.254.169.254/latest/meta-data/public-ipv4)",
        #"aws route53 change-resource-record-sets --hosted-zone-id $zoneid --cli-input-json '{ \"ChangeBatch\": { \"Changes\": [ { \"Action\": \"CREATE\", \"ResourceRecordSet\": { \"Name\": \"'$dns'\", \"Type\": \"A\", \"TTL\": 300, \"ResourceRecords\": [ { \"Value\": \"'$pubIp'\" } ] } } ] } }'"
        ])

def webserver_instance(student, instance_type, sec_groups, keyname):
    instance = Instance(
        "WebServer{}".format(student),
        InstanceType=Ref(instance_type),
        SecurityGroups=[],
        Tags=Tags(
            Name="Web Server {}".format(student),
            Student="Student{}".format(student)),
        KeyName=Ref(keyname),
        ImageId=FindInMap("RegionMap", Ref("AWS::Region"), "AMI"),
        UserData=Base64(webserver_userdata()))
    append_sgs_to_instance(instance, sec_groups)
    return instance

def build_webserver_resources(template, student, instance_type, sec_groups, keyname):
    hostedzone = template.parameters.get("HostedZone")
    event = template.parameters.get("EventId")

    instance = template.add_resource(
        webserver_instance(student, instance_type, sec_groups, keyname))

    recordset = template.add_resource(RecordSetType(
        "DnsName{}".format(student),
        HostedZoneName=Join("", [Ref(hostedzone), "."]),
        Comment="DNS Name for student {}".format(student),
        Name=Join("", ["student", student, ".", Ref(event), ".",
                       Ref(hostedzone), "."]),
        Type="A",
        TTL="300",
        ResourceRecords=[GetAtt(instance.title, "PublicIp")]
        ))

    template.add_output(Output(
        "WebServerDNS{}".format(student),
        Value=Ref(recordset),
        Description="DNS Name of Web Server {}".format(student)))


def build_template(filename, size, display, docker, web):
    """Build a Puppet, Docker or WebServer Cloudformation template based on
    the arguments passed in
    """

    keyname = Parameter(
        "KeyName",
        Description="Name of an existing EC2 KeyPair to enable "
                      "SSH access to the instance",
        Type="String",
        Default="classkey")

    parameters = []
    parameters.append(keyname)

    if docker:
        region = build_region_param(["eu-west-1"], 0)
        instance_type = build_instancetype_param(["m3.medium", "m3.large"], 1)
        region_map = docker_region_map()
        sec_groups = docker_sgs()
        build_resources = build_docker_resources
    if web:
        region = build_region_param(["eu-west-1"], 0)
        instance_type = build_instancetype_param(
            ["t2.micro", "t2.small", "t2.medium"], 1)
        parameters.append(Parameter(
            "HostedZone",
            Description="The DNS name of an existing Amazon Route 53 "
            "hosted zone",
            Type="String"))
        parameters.append(Parameter(
            "EventId",
            Description="The DNS-compliant identifier for this event",
            AllowedPattern="[a-z\d]+",
            ConstraintDescription="Must be all lower case or numeric",
            Type="String"))

        region_map = web_region_map()
        sec_groups = web_sgs()
        build_resources = build_webserver_resources
    else:
        region = build_region_param(
            ['eu-west-1', 'us-east-1', 'ap-southeast-1'], 0)
        instance_type = build_instancetype_param(["m3.medium", "m3.large"], 1)
        region_map = puppet_region_map()
        sec_groups = puppet_sgs()
        build_resources = build_puppet_resources

    parameters.append(instance_type)
    parameters.append(region)

    template = troposphere.Template()
    template.add_version("2010-09-09")
    template.add_description("Builds classroom users for cloud classrooms")

    for param in parameters:
        template.add_parameter(param)
    
    template.add_mapping("RegionMap", region_map)

    for sec_group in sec_groups:
        template.add_resource(sec_group)

    #resources
    for student in range(1, size + 1):
        build_resources(template, student, instance_type, sec_groups, keyname)

    if display:
        print(template.to_json())

    template_file = open(filename, "w")
    template_file.write(template.to_json())

def main():
    """Parse command line args then invoke build_template
    """

    parser = argparse.ArgumentParser()
    parser.add_argument("-t", "--template",
                        help="name for the generated Cloudformation template",
                        default="class.template")
    parser.add_argument("-s", "--size",
                        help="class size",
                        type=int,
                        default=1)
    parser.add_argument("-p", "--print",
                        help="display the template",
                        action="store_true")
    group = parser.add_mutually_exclusive_group()
    group.add_argument("-d", "--docker",
                       help="build docker template",
                       action="store_true")
    group.add_argument("-w", "--webserver",
                       help="Build out a simple webserver template",
                       action="store_true")
    args = parser.parse_args()

    build_template(
        args.template, args.size, args.print, args.docker, args.webserver)



if __name__ == "__main__":
    main()
